import React from "react";

const Card = (props) => {
  return (
      <li className={"card rank-" + props.rank + " " + props.suit}>
            <span className="rank">{props.rank}</span>
            <span className="suit">{props.icon}</span>
      </li>
  );
};

export default Card;