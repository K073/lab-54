import React, { Component } from 'react';
import './App.css';
import Card from "./Card/Card";

class App extends Component {
  constructor () {
    super();
    this.state = {
      arrCards: []
    };

    this.arrRank = [2, 3, 4, 5, 6, 7, 8, 9, 10, "j", "q", "k", "a"];
    this.arrSuit = [
        {suit: "hearts",
        icon: "♥"},
        {suit: "diams",
        icon: "♦"},
        {suit: "clubs",
        icon: "♣"},
        {suit: "spades",
        icon: "♠"}
    ];
  }

  generateCards = () => {
    const arrCards = [];
    const deck = [];
    for (let i = 0; i < this.arrRank.length; i++){
      for (let j = 0; j < this.arrSuit.length; j++){
        let card = {
            rank: this.arrRank[i],
            suit: this.arrSuit[j].suit,
            icon: this.arrSuit[j].icon
        };
        deck.push(card);
      }
    }
    for (let i = 0; i < 5; i++){
      arrCards.push(deck.splice(Math.floor(Math.random() * deck.length - 1), 1)[0]);
    }
      console.log(arrCards);
      this.setState({arrCards})
  };

  render() {
    return (
      <div className="App">
          <button onClick={this.generateCards}>shufle</button>
          <div className="playingCards faceImages">
              <ul>
                  {this.state.arrCards.map((value, index) => {
                      return <Card key={index} rank={value.rank} suit={value.suit} icon={value.icon}/>
                  })}
              </ul>
          </div>
      </div>
    );
  }
}

export default App;
